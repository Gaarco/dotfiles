#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

git_prompt='/usr/share/git/completion/git-prompt.sh'
git_comple='/usr/share/git/completion/git-completion.bash'
[[ -f $git_prompt ]] && source $git_prompt
[[ -f $git_comple ]] && source $git_comple

alias ls='ls --group-directories-first --color=auto'
alias ll='ls -l --block-size=M'
alias c='clear'
alias config='/usr/bin/git --git-dir=$HOME/.dotfiles/.git --work-tree=$HOME'

bind 'set completion-ignore-case on'
bind '"\e0A": history-search-backward'
bind '"\e[A": history-search-backward'
bind '"\e0B": history-search-forward'
bind '"\e[B": history-search-forward'

# Disable dotnet telemetry
export DOTNET_CLI_TELEMETRY_OPTOUT=1

# Customize git script
export GIT_PS1_SHOWDIRTYSTATE=1
export GIT_PS1_SHOWSTASHSTATE=1
export GIT_PS1_SHOWUNTRACKEDFILES=1
export GIT_PS1_SHOWUPSTREAM="verbose"

export PS1='\e[0;32m[\u@\h \w]\e[0;97m$([[ $(declare -Ff __git_ps1) ]] && __git_ps1 " (%s)") \e[0;94m\t\e[0m\n\$ '
export PATH="$PATH:$HOME/.bin/:$HOME/.bin/zig/:$HOME/.bin/zls/zig-out/bin/ "
export MANPAGER='nvim +Man!'
export EDITOR=nvim
