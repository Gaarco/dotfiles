vim.lsp.start({
    name = "lua-language-server",
    cmd = { "lua-language-server" },
    root_dir = vim.fs.dirname(vim.fs.find({ ".luacheckrc", ".stylua.toml", "stylua.toml", ".styluaignore", ".git" }, { upward = true })[1]),
    settings = {
        Lua = {
            completion = {
                autoRequire = false,
            },
            hint = {
                enable = true,
            },
            diagnostics = {
                -- Get the language server to recognize the `vim` global
                globals = { "vim", "love" },
            },
            workspace = {
                -- Make the server aware of Neovim runtime files
                library = vim.api.nvim_get_runtime_file("", true),
                checkThirdParty = false,
            },
            telemetry = {
                enable = false,
            },
        },
    },
})
