vim.lsp.start({
    name = "zls",
    cmd = { "zls" },
    root_dir = vim.fs.dirname(vim.fs.find({ ".git", "build.zig" }, { upward = true })[1]),
})
