require("jdtls").start_or_attach({
    name = "jdtls",
    cmd = {
        "jdtls",
        "--jvm-arg=-Djava.import.generatesMetadataFilesAtProjectRoot=false",
        "--jvm-arg=-javaagent:" .. vim.fs.normalize("~/.cache/jdtls/lombok.jar"),
        "-configuration",
        vim.fs.normalize("~/.cache/jdtls"),
        "-data",
        vim.fn.join({ vim.fs.normalize("~/.cache/jdtls/data"), vim.fs.basename(vim.fn.getcwd()) }, "/"),
    },
    root_dir = vim.fs.dirname(vim.fs.find({
        "build.gradle",
        "build.gradle.kts",
        "build.xml",
        "pom.xml",
        "settings.gradle",
        "settings.gradle.kts",
        ".git",
    }, { upward = true })[1]),
})
