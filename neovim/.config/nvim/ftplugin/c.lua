vim.lsp.start({
    name = "ccls",
    cmd = { "ccls" },
    root_dir = vim.fs.dirname(
        vim.fs.find(
            { "Makefile", ".ccls", ".clangd", ".clang-format", ".clang-tidy", "compile-flags.txt", "compile_commands.json", ".git" },
            { upward = true }
        )[1]
    ),
})
