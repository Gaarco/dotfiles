vim.api.nvim_create_autocmd("BufWritePre", {
    callback = function()
        local pos = vim.api.nvim_win_get_cursor(0)

        vim.cmd([[:keepjumps keeppatterns %s/\s\+$//e]])
        vim.cmd([[:keepjumps keeppatterns silent! 0;/^\%(\n*.\)\@!/,$d]])

        local last_row = vim.api.nvim_buf_line_count(0)

        if pos[1] > last_row then
            pos[1] = last_row
        end

        vim.api.nvim_win_set_cursor(0, pos)
    end,
})

vim.api.nvim_create_autocmd("BufEnter", {
    callback = function()
        if vim.bo.buftype == "terminal" then
            vim.cmd("startinsert!")
        end
    end,
})
