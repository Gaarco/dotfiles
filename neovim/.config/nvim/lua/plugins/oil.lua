return {
    "stevearc/oil.nvim",
    dependencies = { "nvim-tree/nvim-web-devicons" },
    config = function()
        vim.keymap.set("n", "<Leader><Leader>", "<cmd>Oil<cr>", { silent = true, desc = "Map <Leader><Leader> to open Oil" })
        require("oil").setup({})
    end,
}
