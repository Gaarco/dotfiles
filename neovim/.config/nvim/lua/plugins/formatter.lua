return {
    "mhartington/formatter.nvim",
    config = function()
        require("formatter").setup({
            filetype = {
                c = {
                    require("formatter.filetypes.c").clangformat,
                },
                lua = {
                    require("formatter.filetypes.lua").stylua,
                },
                zig = {
                    require("formatter.filetypes.zig").zigfmt,
                },
                ["*"] = {
                    require("formatter.filetypes.any").remove_trailing_whitespace,
                },
            },
        })
        vim.keymap.set("n", ",f", function()
            vim.cmd([[FormatWrite]])
        end, { silent = true, desc = "[F]ormat buffer" })
    end,
}
