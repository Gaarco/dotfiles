return {
    "nvim-lua/plenary.nvim",
    config = function()
        require("plenary.filetype").add_table({
            extension = {
                ["glsl"] = "glsl",
                ["norg"] = "norg",
                ["pdb"] = "prolog",
                ["zig"] = "zig",
            },
        })
    end,
}
