return {
    "windwp/nvim-autopairs",
    config = function()
        local autopairs = require("nvim-autopairs")
        local Rule = require("nvim-autopairs.rule")
        local cond = require("nvim-autopairs.conds")

        autopairs.setup({
            ignored_next_char = "[%w%.]",
            enable_check_bracket_line = true,
            check_ts = true,
        })

        autopairs.add_rules({
            Rule("$", "$", { "tex", "latex", "markdown" }):with_move(cond.done()),
        })
    end,
}
