return {
    "nvim-treesitter/nvim-treesitter",
    dependencies = {
        {
            "nvim-treesitter/nvim-treesitter-context",
            config = function ()
                require("treesitter-context")
            end
        },
    },
    build = ":TSUpdate",
    config = function()
        require("nvim-treesitter.install").compilers = {
            "gcc",
            "zig",
            "cc",
            "clang",
        }

        require("nvim-treesitter.configs").setup({
            ensure_installed = {"c", "lua", "java", "vim", "vimdoc", "markdown", "markdown_inline"},
            auto_install = true,
            highlight = {
                enable = true,
                additional_vim_regex_highlighting = false,
                disable = function (_, buf)
                    local MAX_FILESIZE = 1024 * 1024 -- 1 MB
                    local ok, stats = pcall(vim.loop.fs_stat, vim.api.nvim_buf_get_name(buf))

                    if ok and stats and stats.size > MAX_FILESIZE then
                        return true
                    end
                end
            },
            incremental_selection = {
                enable = true,
                keymaps = {
                    init_selection = "ti",
                    node_incremental = "tn",
                    scope_incremental = "ts",
                    node_decremental = "tp",
                },
            },
            indent = {
                enable = true,
            },
        })
    end,
}
