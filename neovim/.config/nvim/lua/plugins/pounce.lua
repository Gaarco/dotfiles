return {
    "rlane/pounce.nvim",
    config = function()
        vim.keymap.set(
            { "n", "v" },
            "s",
            require("pounce").pounce,
            { desc = "Start pounce search" }
        )

        require("pounce").setup({
            multi_window = false,
        })
    end,
}
