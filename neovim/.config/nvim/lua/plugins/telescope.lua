return {
    "nvim-telescope/telescope.nvim",
    dependencies = {
        "nvim-lua/plenary.nvim",
        {
            "nvim-telescope/telescope-fzf-native.nvim",
            build = "make",
        },
    },
    config = function()
        local telescope = require("telescope")
        local actions = require("telescope.actions")
        telescope.setup({
            defaults = require("telescope.themes").get_ivy({
                file_ignore_patterns = { "%.git/" },
                layout_config = {
                    height = 0.45,
                },
                mappings = {
                    ["i"] = {
                        ["<esc>"] = actions.close,
                        ["<A-x>"] = actions.select_horizontal,
                        ["<A-v>"] = actions.select_vertical,
                    },
                },
            }),
            pickers = {
                find_files = {
                    hidden = true,
                },
                buffers = {
                    mappings = {
                        ["i"] = {
                            ["<A-d>"] = actions.delete_buffer,
                        },
                    },
                },
            },
        })

        telescope.load_extension("fzf")

        local builtins = require("telescope.builtin")

        vim.keymap.set("n", "<Leader>ff", function()
            builtins.find_files()
        end, { desc = "[f]ind [f]iles" })
        vim.keymap.set("n", "<Leader>fg", function()
            builtins.live_grep()
        end, { desc = "[f]ind [g]rep" })
        vim.keymap.set("n", "<Leader>fh", function()
            builtins.help_tags()
        end, { desc = "[f]ind [h]elp" })
        vim.keymap.set("n", "<Leader>fb", function()
            builtins.buffers()
        end, { desc = "[f]ind [b]uffer" })
        vim.keymap.set("n", "<Leader>fd", function()
            builtins.diagnostics()
        end, { desc = "[f]ind [d]iagnostics" })
        vim.keymap.set("n", "<Leader>qf", function()
            builtins.quickfix()
        end, { desc = "[q]uick[f]ix list" })
        vim.keymap.set("n", "<Leader>fr", function()
            builtins.lsp_references()
        end, { desc = "[f]ind [r]eferences" })
        vim.keymap.set("n", "<Leader>fs", function()
            builtins.lsp_document_symbols()
        end, { desc = "[f]ind [s]ymbols" })
        vim.keymap.set("n", "<Leader>fS", function()
            builtins.lsp_workspace_symbols()
        end, { desc = "[f]ind [s]ymbols" })
    end,
}
