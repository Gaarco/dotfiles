return {
    "akinsho/toggleterm.nvim",
    config = function()
        require("toggleterm").setup({
            size = function(term)
                if term.direction == "horizontal" then
                    return 20
                elseif term.direction == "vertical" then
                    return vim.o.columns * 0.45
                end
            end,
            close_on_exit = true,
            hide_numbers = true,
            persist_mode = false,
            persist_size = false,
            shade_terminals = false,
            shell = vim.o.shell,
            start_in_insert = true,
        })

        vim.keymap.set(
            { "n", "t" },
            "<A-t>x",
            "<CMD>ToggleTerm direction=horizontal<CR>",
            { silent = true, noremap = true, desc = "Toggle horizontal terminal" }
        )

        vim.keymap.set(
            { "n", "t" },
            "<A-t>v",
            "<CMD>ToggleTerm direction=vertical<CR>",
            { silent = true, noremap = true, desc = "Toggle vertical terminal" }
        )
    end,
}
