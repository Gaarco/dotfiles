return {
    "ten3roberts/window-picker.nvim",
    config = function()
        require("window-picker").setup({
            swap_shift = false,
        })

        vim.keymap.set("n", "<Leader>ww", "<cmd>WindowPick<cr>", { desc = "Window pick" })
        vim.keymap.set("n", "<Leader>ws", "<cmd>WindowSwap<cr>", { desc = "Swap windows" })
        vim.keymap.set("n", "<Leader>wz", "<cmd>WindowZap<cr>", { desc = "Zap window" })
    end,
}
