-- Enable experimental Lua module loader
vim.loader.enable()

require("settings")
require("autocommands")
require("keymaps")
require("lspconfig")
require("bootstrap")

vim.cmd("colorscheme semplice")
